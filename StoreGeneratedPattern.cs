﻿namespace StoreGeneratedPatternFix
{
    public enum StoreGeneratedPattern
    {
        None = 0,
        Computed = 1,
        Identity = 2
    }
}

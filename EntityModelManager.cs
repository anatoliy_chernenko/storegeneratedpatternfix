﻿using System.Linq;
using System.Xml.Linq;

namespace StoreGeneratedPatternFix
{
    public class EntityModelManager
    {
        public void CopyStoreGeneratedPattern(XDocument document, StoreGeneratedPattern pattern)
        {
            foreach (var element in
                document.Descendants("{http://schemas.microsoft.com/ado/2008/09/edm}Property").Where(
                    x =>
                    x.Attribute("{http://schemas.microsoft.com/ado/2009/02/edm/annotation}StoreGeneratedPattern") !=
                    null &&
                    x.Attribute("{http://schemas.microsoft.com/ado/2009/02/edm/annotation}StoreGeneratedPattern").Value == pattern.ToString()))
            {
                var entityTypeNamespace = element.Parent.Parent.Attribute("Namespace").Value;
                var entityTypeName = element.Parent.Attribute("Name").Value;
                var entityTypeFullName = entityTypeNamespace + "." + entityTypeName;
                var propertyName = element.Attribute("Name").Value;

                var entityTypeMapping =
                    document.Descendants("{http://schemas.microsoft.com/ado/2008/09/mapping/cs}EntityTypeMapping").Where(
                        x =>
                        x.Attribute("TypeName").Value == entityTypeFullName ||
                        x.Attribute("TypeName").Value == "IsTypeOf(" + entityTypeFullName + ")").First();
                var storeEntitySet =
                    entityTypeMapping.Element("{http://schemas.microsoft.com/ado/2008/09/mapping/cs}MappingFragment").
                        Attribute("StoreEntitySet").Value;

                var columnName =
                    entityTypeMapping.Element("{http://schemas.microsoft.com/ado/2008/09/mapping/cs}MappingFragment")
                        .Elements()
                        .Where(x => x.Attribute("Name").Value == propertyName)
                        .First()
                        .Attribute("ColumnName").Value;

                var ssdlEntityType =
                    document.Descendants("{http://schemas.microsoft.com/ado/2009/02/edm/ssdl}EntityType").Where(
                        x => x.Attribute("Name").Value == storeEntitySet).First();

                var ssdlProperty =
                    ssdlEntityType.Elements("{http://schemas.microsoft.com/ado/2009/02/edm/ssdl}Property").Where(
                        x => x.Attribute("Name").Value == columnName).First();

                if (ssdlProperty.Attribute("StoreGeneratedPattern") == null ||
                    ssdlProperty.Attribute("StoreGeneratedPattern").Value != pattern.ToString())
                {
                    ssdlProperty.SetAttributeValue("StoreGeneratedPattern", pattern.ToString());
                }
            }
        }

    }
}
